import { createElement, addClass, removeClass } from "./helper.mjs";

const username = sessionStorage.getItem("username");
const roomPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const roomContainer = document.getElementById("rooms-container");
const createRoomBtn = document.getElementById("roomcreation");
const countElement = document.getElementById("counter-element");

if (!username) {
  window.location.replace("/login");
}

const counter = setInterval((seconds) => {
  if (seconds > 0) {
    countElement.textContent = seconds;
    seconds--;
  } else {
    countElement.classList.add("display-none");
  }
}, 1000);

const socket = io("", { query: { username } });

createRoomBtn.addEventListener("click", () => {
  const newRoomName = prompt("Enter New Room Name");

  socket.emit("ROOM_CREATE", newRoomName);
});

const createRoom = (roomName) => {
  const roomButton = createElement({
    tagName: "button",
    className: "room-item",
    attributes: { name: roomName },
  });
  roomButton.innerHTML = roomName;
  return roomButton;
};

const joinRoom = (el) => {
  console.log(el);
  console.log(el.attributes.name);
  socket.emit("JOIN_GAME_ROOM", el.attributes.name);
};

const addRoomJoinListener = (el) => {
  el.addEventListener("click", () => {
    socket.emit("JOIN_GAME_ROOM", el.textContent);
    // socket.emit("PLAYER_JOIN", el.textContent)
    console.log(el);
    console.log(el.textContent);
  });
};

const roomListUpdate = (rooms) => {
  console.log(rooms);
  const allRooms = rooms.map((x) => createRoom(x));
  console.log(allRooms);
  roomContainer.innerHTML = "";
  roomContainer.append(...allRooms);
  allRooms.map((x) => addRoomJoinListener(x));
};

const gameFieldActivation = () => {
  roomPage.classList.add("display-none");
  gamePage.classList.remove("display-none");

  const playersContainer = createElement({
    tagName: "div",
    className: "players-container",
  });
  const fieldSection = createElement({
    tagName: "div",
    className: "game-container",
  });
  const btnBackToRoom = createElement({ tagName: "button" });
  const btnReady = createElement({ tagName: "button" });
  btnBackToRoom.textContent = "Back To Rooms";
  btnReady.textContent = "Not Ready";

  fieldSection.append(btnReady);
  gamePage.appendChild(btnBackToRoom);
  gamePage.appendChild(playersContainer);
  gamePage.appendChild(fieldSection);
};

const play = () => {
   //playersplacehold
}

const gameOver = setTimeout(play, matchTime)

const winnerBeforeTimesUp = () => {
    clearTimeout(gameOver);
}

socket.on("UPDATE_ROOMS_LIST", roomListUpdate);
socket.on("BATTLEFIELD_ENTER", gameFieldActivation);
socket.on("NEW_PLAYER_JOINED");
