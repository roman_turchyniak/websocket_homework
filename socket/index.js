import * as config from "./config";

var rooms = [];

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;

    socket.emit("UPDATE_ROOMS_LIST", rooms);

    socket.on("ROOM_CREATE", (newRoom) => {
      if (newRoom && !rooms.find((el) => el === newRoom)) {
        rooms.push(newRoom);
      }

      io.emit("UPDATE_ROOMS_LIST", rooms);
    });

    socket.on("JOIN_GAME_ROOM", (room) => {
      socket.join(room);

      let partySize = io.clients((error, clients) => {
        if (error) throw error;
        console.log(clients.username);
      });

      //console.log(partySize);

      if (partySize <= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.join(room);
        socket.emit("BATTLEFIELD_ENTER", partySize);
      } else {
        socket.emit("ROOM_FULL");
      }
    });
  });
};
